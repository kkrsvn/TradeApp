//
//  HomeViewModel.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 19.03.2023.
//

import UIKit

protocol HomeViewModelProtocol: AnyObject {
    var networkService: NetworkServiceProtocol { get set }
    func updateHomeView()
}

class HomeViewModel: HomeViewModelProtocol {
    
    var networkService: NetworkServiceProtocol
    
    required init(networkService: NetworkServiceProtocol) {
        self.networkService = networkService
    }
    
    func updateHomeView() {
        self.networkService.fetchData(withURL: latestDealsURL, forRequestType: .latestDeals)
        self.networkService.fetchData(withURL: flashSaleURL, forRequestType: .flashSale)
    }
}
