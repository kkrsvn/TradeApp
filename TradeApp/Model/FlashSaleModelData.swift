//
//  FlashSaleModelData.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 17.03.2023.
//

import Foundation

// MARK: - FlashSale
struct FlashSaleData: Codable {
    let flashSale: [FlashSaleElement]

    enum CodingKeys: String, CodingKey {
        case flashSale = "flash_sale"
    }
}

// MARK: - FlashSaleElement
struct FlashSaleElement: Codable {
    let category, name: String
    let price: Double
    let discount: Int
    let imageURL: String

    enum CodingKeys: String, CodingKey {
        case category, name, price, discount
        case imageURL = "image_url"
    }
}

// MARK: - Flash Sale Model
struct FlashSaleModel {
    var flashSaleElements: [(title : String, category : String, price : Double, discount : Int, imageURL : String)] = []
    
    init?(flashSaleData: FlashSaleData) {
        for element in flashSaleData.flashSale {
            self.flashSaleElements.append((title: element.name,
                                           category: element.category,
                                           price: element.price,
                                           discount: element.discount,
                                           imageURL: element.imageURL))
        }
    }
}
