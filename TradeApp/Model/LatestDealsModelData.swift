//
//  LatestDealsModelData.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 17.03.2023.
//

import Foundation

// MARK: - LatestDeals
struct LatestDealsData: Codable {
    let latest: [Latest]
}

// MARK: - Latest
struct Latest: Codable {
    let category, name: String
    let price: Int
    let imageURL: String

    enum CodingKeys: String, CodingKey {
        case category, name, price
        case imageURL = "image_url"
    }
}

// MARK: - Latest Deals Model
struct LatestDealsModel {
    var latestElements: [(title : String, category : String, price : Int, imageURL : String)] = []
    
    init?(latestDealsData: LatestDealsData) {
        for element in latestDealsData.latest {
            self.latestElements.append((title: element.name,
                                        category: element.category,
                                        price: element.price,
                                        imageURL: element.imageURL))
        }
    }
}
