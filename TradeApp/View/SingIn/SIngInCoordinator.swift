//
//  SIngInCoordinator.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 28.03.2023.
//

import UIKit

class SingInCoordinator: CoordinatorProtocol, ChildrenCoordinatingProtocol {
    
    var navigationController: UINavigationController?
    var mainCoordinator: MainCoordinatorProtocol?
    var childCoordinators: [CoordinatorProtocol] = []
    
    init(navigationController: UINavigationController?, mainCoordinator: MainCoordinatorProtocol?) {
        self.navigationController = navigationController
        self.mainCoordinator = mainCoordinator
    }
    
    func start() {
        let singVC: UIViewController & CoordinatingProtocol = ModuleBuilder.createSingInModule(coordinator: self)
        self.navigationController?.setViewControllers([singVC], animated: true)
    }
}
