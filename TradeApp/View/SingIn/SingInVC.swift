//
//  SingInVC.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 09.03.2023.
//

import UIKit
import CoreData

class SingInVC: UIViewController, CoordinatingProtocol {
    
    var persons: [Person] = []
    var context: NSManagedObjectContext!
    var coordinator: ChildrenCoordinatingProtocol?

    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var userAlreadyExistLable: UILabel!
    
    required init(coordinator: ChildrenCoordinatingProtocol?) {
        self.coordinator = coordinator
        super .init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setNavigationBar()
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()
        
        do {
            persons = try self.context.fetch(fetchRequest)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    private func setNavigationBar() {
        self.navigationItem.backButtonTitle = "Назад"
        self.navigationController?.navigationBar.isHidden = true
    }
    
    private func blinkUserAlreadyExistLable() {
        self.userAlreadyExistLable.isHidden = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.userAlreadyExistLable.isHidden = true
        }
    }
    
    @IBAction func singInTapButton(_ sender: Any) {
        guard let firstName = firstNameTextField.text,
              spaceValidate(string: firstName) else {
            isEmptyTextField(self.firstNameTextField, 0.35)
            return
        }
        guard let lastName = lastNameTextField.text,
              spaceValidate(string: lastName) else {
            isEmptyTextField(self.lastNameTextField, 0.35)
            return
        }
        guard let email = emailTextField.text,
              spaceValidate(string: email),
              emailValidate(for: email) else {
            isEmptyTextField(self.emailTextField, 0.35)
            return
        }
        
        guard !self.persons.contains(where: { person in
            (person as Person).firstName == firstName
        }) else {
            self.blinkUserAlreadyExistLable()
            return
        }
        
        guard let entity = NSEntityDescription.entity(forEntityName: "Person", in: context) else { return }
        
        let personObject = Person(entity: entity, insertInto: context)
        personObject.firstName = firstName
        personObject.secondName = lastName
        personObject.email = email

        do {
            try self.context.save()
            persons.append(personObject)
        } catch let error as NSError {
            printContent(error.localizedDescription)
        }
        
        self.firstNameTextField.text = nil
        self.lastNameTextField.text = nil
        self.emailTextField.text = nil
    }
    
    @IBAction func logInTapButton(_ sender: Any) {
        self.coordinator?.mainCoordinator?.eventOccured(with: .moveToLoginVC)
    }
}

