//
//  ProfileCoordinator.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 29.03.2023.
//

import UIKit

class ProfileCoordinator: CoordinatorProtocol, TabBarCoordinatingProtocol {
    
    var navigationController: UINavigationController?
    var tabBarCoordinator: TabBarCoordinatorProtocol?
    var childCoordinators: [CoordinatorProtocol] = []
    
    init(navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }
    
    func start() {
        let profileVC: UIViewController & ItemCoordinatingProtocol = ProfileVC(profileTableDataSource: profileDataSource, itemCoordinator: self)
        self.navigationController?.setViewControllers([profileVC], animated: true)
    }
}
