//
//  ProfileTableViewCell.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 12.03.2023.
//

import UIKit

protocol TakeTitleCellProtocol {
    func takeTitleCell() -> String
}

protocol ProfileTableViewCellPorotocol {
    var icon: String { get set }
    var lable: String { get set }
    var isHideArrow: Bool { get set }
    var isHideBalance: Bool { get set }
}

protocol ProfileSetupable {
    func setup(with viewModel: ProfileTableViewCellPorotocol)
}

class ProfileTableViewCell: UITableViewCell, TakeTitleCellProtocol {
    
    struct ProfileTableCell: ProfileTableViewCellPorotocol {
        var icon: String
        var lable: String
        var isHideArrow: Bool
        var isHideBalance: Bool
    }
    
    private lazy var backgroundIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.image = UIImage(systemName: "circle.fill", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 40)))
        imageView.tintColor = UIColor(red: 0.933, green: 0.937, blue: 0.957, alpha: 1)
        return imageView
    }()
    
    private lazy var backgroundIconConstraints = [
        self.backgroundIcon.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor),
        self.backgroundIcon.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor)
    ]
    
    private lazy var iconImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.tintColor = UIColor(red: 0.016, green: 0.016, blue: 0.008, alpha: 1)
        return imageView
    }()
    
    private lazy var iconImageConstraints = [
        self.iconImage.centerYAnchor.constraint(equalTo: self.backgroundIcon.centerYAnchor),
        self.iconImage.centerXAnchor.constraint(equalTo: self.backgroundIcon.centerXAnchor)
    ]

    private lazy var categoryLable: UILabel = {
        let lable = UILabel()
        lable.font = UIFont(name: "MarkPro-Medium", size: 13)
        lable.textColor = UIColor(red: 0.016, green: 0.016, blue: 0.008, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var lableConstraints = [
        self.categoryLable.leadingAnchor.constraint(equalTo: self.backgroundIcon.trailingAnchor, constant: 17),
        self.categoryLable.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor)
    ]
    
    private lazy var arrowImage: UIImageView = {
        let image = UIImage(systemName: "control", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 15.0)))
        let imageView = UIImageView()
        imageView.image = image
        imageView.transform = imageView.transform.rotated(by: .pi / 2)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.tintColor = UIColor(red: 0.016, green: 0.016, blue: 0.008, alpha: 1)
        return imageView
    }()
    
    private lazy var arrowImageConstraints = [
        self.arrowImage.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -13),
        self.arrowImage.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor)
    ]
    
    private lazy var balanceLable: UILabel = {
        let lable = UILabel()
        lable.text = "$ 1593"
        lable.font = UIFont(name: "MarkPro-Medium", size: 13)
        lable.textColor = UIColor(red: 0.016, green: 0.016, blue: 0.008, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .right
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var balanceLableConstraints = [
        self.balanceLable.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -13),
        self.balanceLable.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor)
    ]

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    private func setupView() {
        self.backgroundColor = UIColor(red: 0.98, green: 0.976, blue: 1, alpha: 1)
        self.contentView.addSubview(self.backgroundIcon)
        self.contentView.addSubview(self.iconImage)
        self.contentView.addSubview(self.categoryLable)
        self.contentView.addSubview(self.arrowImage)
        self.contentView.addSubview(self.balanceLable)
        self.setupConstraints()
    }

    private func setupConstraints() {
        NSLayoutConstraint.activate(self.backgroundIconConstraints)
        NSLayoutConstraint.activate(self.iconImageConstraints)
        NSLayoutConstraint.activate(self.lableConstraints)
        NSLayoutConstraint.activate(self.arrowImageConstraints)
        NSLayoutConstraint.activate(self.balanceLableConstraints)
    }
    
    func takeTitleCell() -> String {
        return self.categoryLable.text ?? ""
    }
}

extension ProfileTableViewCell: ProfileSetupable {
    func setup(with viewModel: ProfileTableViewCellPorotocol) {
        guard let viewModel = viewModel as? ProfileTableCell else { return }
        self.iconImage.image = UIImage(systemName: viewModel.icon, withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 22)))
        self.categoryLable.text = viewModel.lable
        self.arrowImage.isHidden = viewModel.isHideArrow
        self.balanceLable.isHidden = viewModel.isHideBalance
    }
}
