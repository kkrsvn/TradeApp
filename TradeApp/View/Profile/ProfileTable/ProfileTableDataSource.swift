//
//  ProfileTableDataSource.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 21.03.2023.
//

import UIKit

extension ProfileVC: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 64
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as? ProfileTableViewCell else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DefaultCell", for: indexPath)
            return cell
        }
        let data = self.profileTableDataSource[indexPath.row]
        
        let viewModel = ProfileTableViewCell.ProfileTableCell(icon: data.icon,
                                                              lable: data.lable,
                                                              isHideArrow: data.isHideArrow,
                                                              isHideBalance: data.isHideBalance)
        cell.setup(with: viewModel)
        cell.selectionStyle = .none
        return cell
    }
}
