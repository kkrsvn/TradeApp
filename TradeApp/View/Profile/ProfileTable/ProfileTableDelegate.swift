//
//  ProfileTableDelegate.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 21.03.2023.
//

import UIKit

extension ProfileVC: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? TakeTitleCellProtocol {
            cell.takeTitleCell() == "Log Out" ? self.itemCoordinator?.tabBarCoordinator?.mainCoordinator?.eventOccured(with: .moveToSingInVC) : ()
        }
    }
}
