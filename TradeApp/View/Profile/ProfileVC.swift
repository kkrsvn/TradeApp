//
//  ProfileVC.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 12.03.2023.
//

import UIKit

class ProfileVC: UIViewController, ItemCoordinatingProtocol {
    
    var itemCoordinator: TabBarCoordinatingProtocol?
    var profileTableDataSource: [ProfileData]

    @IBOutlet weak var profileTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.profileCellRegistry()
        self.profileTableDataSource = profileDataSource
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    required init(profileTableDataSource: [ProfileData], itemCoordinator: TabBarCoordinatingProtocol?) {
        self.profileTableDataSource = profileTableDataSource
        self.itemCoordinator = itemCoordinator
        super .init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func profileCellRegistry() {
        self.profileTableView.register(UITableViewCell.self, forCellReuseIdentifier: "DefaultCell")
        self.profileTableView.register(ProfileTableViewCell.self, forCellReuseIdentifier: "ProfileTableViewCell")
    }

    @IBAction func tapBackButton(_ sender: Any) {
        self.itemCoordinator?.tabBarCoordinator?.tabBarController.selectedIndex = 0
    }
}

