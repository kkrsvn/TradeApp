//
//  HomeView.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 13.03.2023.
//

import UIKit

protocol HomeViewDelegateProtocol: AnyObject {
    func didTapNavigationButton()
}

class HomeView: UIView {
    
    weak var delegate: HomeViewDelegateProtocol?
    
    // MARK: - Navigation Button
    private lazy var navigationButton: UIButton = {
        let image = UIImage(named: "navigationImage")
        
        let button = UIButton()
        button.setImage(image, for: .normal)
        button.imageView?.tintColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
        button.addTarget(self, action: #selector(self.tapNavigationButton), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var navigationButtonConstraints = [
        self.navigationButton.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 15),
        self.navigationButton.centerYAnchor.constraint(equalTo: self.titleVCLable.centerYAnchor),
        self.navigationButton.widthAnchor.constraint(equalToConstant: 24),
        self.navigationButton.heightAnchor.constraint(equalToConstant: 24)
    ]
    
    // MARK: - Title
    private lazy var titleVCLable: UILabel = {
        let lable = UILabel()
        lable.text = "Trade by"
        lable.font = UIFont(name: "MarkPro-Heavy", size: 30)
        lable.textColor = .black
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var titleVCLableConstraints = [
        self.titleVCLable.topAnchor.constraint(equalTo: self.topAnchor, constant: 60),
        self.titleVCLable.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 100)
    ]
    
    // MARK: - Title Name
    private lazy var titleNameLable: UILabel = {
        let lable = UILabel()
        lable.text = "bata"
        lable.font = UIFont(name: "MarkPro-Heavy", size: 30)
        lable.textColor = UIColor(red: 0.306, green: 0.333, blue: 0.843, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var titleNameLableConstraints = [
        self.titleNameLable.centerYAnchor.constraint(equalTo: self.titleVCLable.centerYAnchor),
        self.titleNameLable.leadingAnchor.constraint(equalTo: self.titleVCLable.trailingAnchor, constant: 10)
    ]
    
    // MARK: - Profile Icon
    private lazy var profileIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "photo")
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 20
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = CGColor(red: 0.309, green: 0.302, blue: 0.302, alpha: 1)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var profileIconConstraints = [
        self.profileIcon.centerYAnchor.constraint(equalTo: self.titleVCLable.centerYAnchor),
        self.profileIcon.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -47),
        self.profileIcon.heightAnchor.constraint(equalToConstant: 40),
        self.profileIcon.widthAnchor.constraint(equalToConstant: 40)
    ]
    
    // MARK: - Location Lable
    private lazy var locationLable: UILabel = {
        let lable = UILabel()
        lable.text = "Location"
        lable.font = UIFont(name: "MarkPro-Regular", size: 10)
        lable.textColor = UIColor(red: 0.357, green: 0.357, blue: 0.357, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var locationLableConstraints = [
        self.locationLable.topAnchor.constraint(equalTo: self.profileIcon.bottomAnchor, constant: 10),
        self.locationLable.centerXAnchor.constraint(equalTo: self.profileIcon.centerXAnchor, constant: -5)
    ]
    
    // MARK: - Location Button
    private lazy var locationButton: UIButton = {
        let image = UIImage(systemName: "control", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 10)))
        
        let button = UIButton()
        button.setImage(image, for: .normal)
        
        if let image = button.imageView {
            button.imageView?.transform = image.transform.rotated(by: .pi)
            button.imageView?.tintColor = UIColor(red: 0.357, green: 0.357, blue: 0.357, alpha: 1)
        }
        
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var locationButtonConstraints = [
        self.locationButton.leadingAnchor.constraint(equalTo: self.locationLable.trailingAnchor, constant: 5),
        self.locationButton.centerYAnchor.constraint(equalTo: self.locationLable.centerYAnchor, constant: 1)
    ]
    
    // MARK: - Search TextField
    private lazy var searchTextField: UITextField = {
        let textField = UITextField()
        textField.font = UIFont(name: "MarkPro-Regular", size: 12)
        textField.textColor = .black
        textField.backgroundColor = UIColor(red: 0.961, green: 0.965, blue: 0.969, alpha: 1)
        textField.attributedPlaceholder = NSAttributedString(string: "What are you looking for?", attributes: [NSAttributedString.Key.foregroundColor: UIColor.black])
        textField.autocapitalizationType = .none
        textField.clearButtonMode = UITextField.ViewMode.never
        textField.textAlignment = .center
        textField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        textField.clipsToBounds = true
        textField.layer.cornerRadius = 15
        textField.translatesAutoresizingMaskIntoConstraints = false
        return textField
    }()
    
    private lazy var searchTextFieldConstraints = [
        self.searchTextField.topAnchor.constraint(equalTo: self.titleVCLable.bottomAnchor, constant: 34),
        self.searchTextField.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 56),
        self.searchTextField.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -56),
        self.searchTextField.heightAnchor.constraint(equalToConstant: 30)
    ]
    
    // MARK: - Search Icon
    private lazy var searchIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.tintColor = .black
        imageView.image = UIImage(systemName: "magnifyingglass", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 12)))
        return imageView
    }()
    
    private lazy var searchIconConstraints = [
        self.searchIcon.centerYAnchor.constraint(equalTo: self.searchTextField.centerYAnchor),
        self.searchIcon.trailingAnchor.constraint(equalTo: self.searchTextField.trailingAnchor, constant: -18)
    ]
    
    // MARK: - ScrollView
    lazy var homeScrollView: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsVerticalScrollIndicator = false
        scroll.backgroundColor = UIColor(red: 0.98, green: 0.976, blue: 1, alpha: 1)
        scroll.translatesAutoresizingMaskIntoConstraints = false
        scroll.contentSize.height = 550
        return scroll
    }()
    
    private lazy var homeScrollViewConstraints = [
        self.homeScrollView.leadingAnchor.constraint(equalTo: self.leadingAnchor),
        self.homeScrollView.trailingAnchor.constraint(equalTo: self.trailingAnchor),
        self.homeScrollView.topAnchor.constraint(equalTo: self.searchTextField.bottomAnchor, constant: 10),
        self.homeScrollView.bottomAnchor.constraint(equalTo: self.bottomAnchor,constant: -63)
    ]
    
    // MARK: - Latest Deals Title
    private lazy var latestDealsLable: UILabel = {
        let lable = UILabel()
        lable.text = "Latest deals"
        lable.font = UIFont(name: "MarkPro-Heavy", size: 20)
        lable.textColor = UIColor(red: 0.016, green: 0.016, blue: 0.008, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var latestDealsLableConstraints = [
        self.latestDealsLable.topAnchor.constraint(equalTo: self.homeScrollView.topAnchor, constant: 100),
        self.latestDealsLable.leadingAnchor.constraint(equalTo: self.homeScrollView.leadingAnchor, constant: 12)
    ]
    
    // MARK: - Latest Deals View All
    private lazy var latestDealsViewAllLable: UILabel = {
        let lable = UILabel()
        lable.text = "View all"
        lable.font = UIFont(name: "MarkPro-Regular", size: 12)
        lable.textColor = UIColor(red: 0.502, green: 0.502, blue: 0.502, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var latestDealsViewAllLableConstraints = [
        self.latestDealsViewAllLable.centerYAnchor.constraint(equalTo: self.latestDealsLable.centerYAnchor),
        self.latestDealsViewAllLable.trailingAnchor.constraint(equalTo: self.homeScrollView.safeAreaLayoutGuide.trailingAnchor, constant: -13)
    ]
    
    // MARK: - Flash Sale Title
    private lazy var flashSaleLable: UILabel = {
        let lable = UILabel()
        lable.text = "Flash Sale"
        lable.font = UIFont(name: "MarkPro-Heavy", size: 20)
        lable.textColor = UIColor(red: 0.016, green: 0.016, blue: 0.008, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var flashSaleLableConstraints = [
        self.flashSaleLable.topAnchor.constraint(equalTo: self.homeScrollView.topAnchor, constant: 286),
        self.flashSaleLable.leadingAnchor.constraint(equalTo: self.homeScrollView.leadingAnchor, constant: 12)
    ]
    
    // MARK: - Flash Sale View All
    private lazy var flashSaleViewAllLable: UILabel = {
        let lable = UILabel()
        lable.text = "View all"
        lable.font = UIFont(name: "MarkPro-Regular", size: 12)
        lable.textColor = UIColor(red: 0.502, green: 0.502, blue: 0.502, alpha: 1)
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var flashSaleViewAllLableConstraints = [
        self.flashSaleViewAllLable.centerYAnchor.constraint(equalTo: self.flashSaleLable.centerYAnchor),
        self.flashSaleViewAllLable.trailingAnchor.constraint(equalTo: self.homeScrollView.safeAreaLayoutGuide.trailingAnchor, constant: -13)
    ]
    
    // MARK: - MainView Init
    override init(frame: CGRect) {
        super.init(frame: .zero)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - MainView Setup
    private func setupView() {
        self.backgroundColor = UIColor(red: 0.98, green: 0.976, blue: 1, alpha: 1)
        self.addedMainViewSubviews()
        self.addedScrollViewSubviews()
        self.setupConstraints()
    }
    
    // MARK: - AddSubview functions
    private func addedMainViewSubviews() {
        self.addSubview(self.titleVCLable)
        self.addSubview(self.titleNameLable)
        self.addSubview(self.navigationButton)
        self.addSubview(self.profileIcon)
        self.addSubview(self.locationLable)
        self.addSubview(self.locationButton)
        self.addSubview(self.searchTextField)
        self.addSubview(self.searchIcon)
        self.addSubview(self.homeScrollView)
    }
    
    private func addedScrollViewSubviews() {
        self.homeScrollView.addSubview(self.latestDealsLable)
        self.homeScrollView.addSubview(self.latestDealsViewAllLable)
        self.homeScrollView.addSubview(self.flashSaleLable)
        self.homeScrollView.addSubview(self.flashSaleViewAllLable)
    }
    
    // MARK: - Constraints Activation
    private func setupConstraints() {
        NSLayoutConstraint.activate(self.titleVCLableConstraints)
        NSLayoutConstraint.activate(self.titleNameLableConstraints)
        NSLayoutConstraint.activate(self.navigationButtonConstraints)
        NSLayoutConstraint.activate(self.profileIconConstraints)
        NSLayoutConstraint.activate(self.locationLableConstraints)
        NSLayoutConstraint.activate(self.locationButtonConstraints)
        NSLayoutConstraint.activate(self.searchTextFieldConstraints)
        NSLayoutConstraint.activate(self.searchIconConstraints)
        NSLayoutConstraint.activate(self.homeScrollViewConstraints)
        NSLayoutConstraint.activate(self.latestDealsLableConstraints)
        NSLayoutConstraint.activate(self.latestDealsViewAllLableConstraints)
        NSLayoutConstraint.activate(self.flashSaleLableConstraints)
        NSLayoutConstraint.activate(self.flashSaleViewAllLableConstraints)
    }
    
    @objc private func tapNavigationButton(sender: UIButton) {
        self.delegate?.didTapNavigationButton()
    }
}
