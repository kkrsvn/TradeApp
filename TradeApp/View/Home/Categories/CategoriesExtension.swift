//
//  CategoriesExtension.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 16.03.2023.
//

import UIKit

extension HomeVC {
    
    func makeCategoriesCollectionLayout() -> UICollectionViewFlowLayout{
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 20
        layout.itemSize = CGSize(width: 50, height: 80)
        return layout
    }
    
    func makeCategoriesCollectionView() -> UICollectionView {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: self.categoriesCollectionLayout)
        collection.delegate = self
        collection.dataSource = self
        collection.backgroundColor = .clear
        collection.showsHorizontalScrollIndicator = false
        collection.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "DefaultCell")
        collection.register(CategoriesCell.self, forCellWithReuseIdentifier: "CategoriesCell")
        collection.translatesAutoresizingMaskIntoConstraints = false
        
        self.homeView.homeScrollView.addSubview(collection)

        collection.topAnchor.constraint(equalTo: self.homeView.homeScrollView.topAnchor, constant: 7).isActive = true
        collection.trailingAnchor.constraint(equalTo: self.homeView.homeScrollView.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        collection.leadingAnchor.constraint(equalTo: self.homeView.homeScrollView.leadingAnchor, constant: 20).isActive = true
        collection.heightAnchor.constraint(equalToConstant: 80).isActive = true
        
        return collection
    }
}
