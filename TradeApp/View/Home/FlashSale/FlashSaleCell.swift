//
//  FlashSaleCell.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 20.03.2023.
//

import UIKit

protocol FlashSaleCellProtocol {
    var title: String { get set }
    var category: String { get set }
    var picture: String { get set }
    var price: Double { get set }
    var discount: Int { get set }
}

protocol FlashSaleSetupableProtocol {
    func setup(with viewModel: FlashSaleCellProtocol)
}

class FlashSaleCell: UICollectionViewCell {
    
    struct FlashSaleCollection: FlashSaleCellProtocol {
        var title: String
        var category: String
        var picture: String
        var price: Double
        var discount: Int
    }
    
    // MARK: - Profile Icon
    private lazy var sallerIcon: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "sallerImage")
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 12
        imageView.layer.borderWidth = 1
        imageView.layer.borderColor = CGColor(red: 0.725, green: 0.725, blue: 0.725, alpha: 1)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var sallerIconConstraints = [
        self.sallerIcon.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 7.5),
        self.sallerIcon.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 7.5),
        self.sallerIcon.heightAnchor.constraint(equalToConstant: 24),
        self.sallerIcon.widthAnchor.constraint(equalToConstant: 24)
    ]
    
    // MARK: Background Image
    private lazy var mainImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private lazy var mainImageConstraints = [
        self.mainImage.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 0),
        self.mainImage.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 0),
        self.mainImage.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0),
        self.mainImage.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: 0)
    ]
    
    // MARK: Background Discount ImageView
    private lazy var discountBackgroundImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor(red: 0.976, green: 0.227, blue: 0.227, alpha: 1)
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 6
        return imageView
    }()
    
    private lazy var discountBackgroundImageConstraints = [
        self.discountBackgroundImage.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 7),
        self.discountBackgroundImage.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -8),
        self.discountBackgroundImage.heightAnchor.constraint(equalToConstant: 17),
        self.discountBackgroundImage.widthAnchor.constraint(equalToConstant: 50)
    ]
    
    // MARK: Sale Lable
    private lazy var discountLable: UILabel = {
        let lable = UILabel()
        lable.font = UIFont(name: "MarkPro-Bold", size: 10)
        lable.textColor = .white
        lable.numberOfLines = 0
        lable.textAlignment = .center
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var discountLableConstraints = [
        self.discountLable.centerYAnchor.constraint(equalTo: self.discountBackgroundImage.centerYAnchor),
        self.discountLable.centerXAnchor.constraint(equalTo: self.discountBackgroundImage.centerXAnchor)
    ]
    
    // MARK: Background Category ImageView
    private lazy var categoryBackgroundImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor(red: 0.769, green: 0.769, blue: 0.769, alpha: 0.85)
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 6
        return imageView
    }()
    
    private lazy var categoryBackgroundImageConstraints = [
        self.categoryBackgroundImage.bottomAnchor.constraint(equalTo: self.titleLable.topAnchor, constant: -16),
        self.categoryBackgroundImage.leadingAnchor.constraint(equalTo: self.priceLable.leadingAnchor),
        self.categoryBackgroundImage.heightAnchor.constraint(equalToConstant: 17),
        self.categoryBackgroundImage.widthAnchor.constraint(equalToConstant: 50)
    ]
    
    // MARK: Category Lable
    private lazy var categoryLable: UILabel = {
        let lable = UILabel()
        lable.font = UIFont(name: "MarkPro-Bold", size: 10)
        lable.textColor = .black
        lable.numberOfLines = 0
        lable.textAlignment = .center
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var categoryLableConstraints = [
        self.categoryLable.centerYAnchor.constraint(equalTo: self.categoryBackgroundImage.centerYAnchor),
        self.categoryLable.centerXAnchor.constraint(equalTo: self.categoryBackgroundImage.centerXAnchor)
    ]
    
    // MARK: Title Lable
    private lazy var titleLable: OutlinedLabel = {
        let lable = OutlinedLabel()
        lable.font = UIFont(name: "MarkPro-Bold", size: 16)
        lable.textColor = .white
        lable.outlineColor = .black
        lable.outlineWidth = 1
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var titleLableConstraints = [
        self.titleLable.bottomAnchor.constraint(equalTo: self.priceLable.topAnchor, constant: -16),
        self.titleLable.leadingAnchor.constraint(equalTo: self.priceLable.leadingAnchor)
    ]
    
    // MARK: Price Lable
    private lazy var priceLable: OutlinedLabel = {
        let lable = OutlinedLabel()
        lable.font = UIFont(name: "MarkPro-Bold", size: 12)
        lable.textColor = .white
        lable.outlineColor = .black
        lable.outlineWidth = 1
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var priceLableConstraints = [
        self.priceLable.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -16),
        self.priceLable.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 10)
    ]
    
    // MARK: Add Button
    private lazy var addButton: UIButton = {
        let image = UIImage(systemName: "plus", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 17.5)))
        
        let button = UIButton()
        button.clipsToBounds = true
        button.layer.cornerRadius = 17.5
        button.backgroundColor = UIColor(red: 0.898, green: 0.914, blue: 0.937, alpha: 1)
        button.setImage(image, for: .normal)
        button.imageView?.tintColor = UIColor(red: 0.329, green: 0.333, blue: 0.537, alpha: 1)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var addButtonConstraints = [
        self.addButton.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -7),
        self.addButton.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -4),
        self.addButton.widthAnchor.constraint(equalToConstant: 35),
        self.addButton.heightAnchor.constraint(equalToConstant: 35)
    ]
    
    // MARK: Like Button
    private lazy var likeButton: UIButton = {
        let image = UIImage(systemName: "heart", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 14)))
        
        let button = UIButton()
        button.clipsToBounds = true
        button.layer.cornerRadius = 14
        button.backgroundColor = UIColor(red: 0.898, green: 0.914, blue: 0.937, alpha: 1)
        button.setImage(image, for: .normal)
        button.imageView?.tintColor = UIColor(red: 0.329, green: 0.333, blue: 0.537, alpha: 1)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var likeButtonConstraints = [
        self.likeButton.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -10),
        self.likeButton.trailingAnchor.constraint(equalTo: self.addButton.leadingAnchor, constant: -5),
        self.likeButton.widthAnchor.constraint(equalToConstant: 28),
        self.likeButton.heightAnchor.constraint(equalToConstant: 28)
    ]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentViewSets()
        self.setupView()
        self.setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        self.contentView.addSubview(self.mainImage)
        self.contentView.addSubview(self.categoryBackgroundImage)
        self.contentView.addSubview(self.categoryLable)
        self.contentView.addSubview(self.titleLable)
        self.contentView.addSubview(self.priceLable)
        self.contentView.addSubview(self.discountBackgroundImage)
        self.contentView.addSubview(self.discountLable)
        self.contentView.addSubview(self.sallerIcon)
        self.contentView.addSubview(self.addButton)
        self.contentView.addSubview(self.likeButton)
    }

    private func setupConstraints() {
        NSLayoutConstraint.activate(self.mainImageConstraints)
        NSLayoutConstraint.activate(self.priceLableConstraints)
        NSLayoutConstraint.activate(self.titleLableConstraints)
        NSLayoutConstraint.activate(self.categoryBackgroundImageConstraints)
        NSLayoutConstraint.activate(self.categoryLableConstraints)
        NSLayoutConstraint.activate(self.discountBackgroundImageConstraints)
        NSLayoutConstraint.activate(self.discountLableConstraints)
        NSLayoutConstraint.activate(self.sallerIconConstraints)
        NSLayoutConstraint.activate(self.addButtonConstraints)
        NSLayoutConstraint.activate(self.likeButtonConstraints)
    }
    
    private func contentViewSets() {
        self.contentView.clipsToBounds = true
        self.contentView.layer.cornerRadius = 16
    }
    
    private func showSubviews() {
        self.categoryBackgroundImage.isHidden = false
        self.categoryLable.isHidden = false
        self.titleLable.isHidden = false
        self.priceLable.isHidden = false
        self.addButton.isHidden = false
        self.discountBackgroundImage.isHidden = false
        self.discountLable.isHidden = false
        self.sallerIcon.isHidden = false
        self.likeButton.isHidden = false
    }
}

extension FlashSaleCell: FlashSaleSetupableProtocol {
    func setup(with flashViewModel: FlashSaleCellProtocol) {
        guard let model = flashViewModel as? FlashSaleCollection else { return }
        self.categoryLable.text = model.category
        self.titleLable.text = model.title
        self.priceLable.text = "$" + String(model.price)
        self.discountLable.text = String(model.discount) + "% off"
        guard let pictureURL = URL(string: model.picture) else { return }
        self.mainImage.load(url: pictureURL)
        self.showSubviews()
    }
}

