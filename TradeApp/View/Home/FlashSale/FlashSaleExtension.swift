//
//  FlashSaleExtension.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 20.03.2023.
//

import UIKit

extension HomeVC {
    
    func makeFlashSaleLayout() -> UICollectionViewFlowLayout{
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 10
        layout.itemSize = CGSize(width: (UIScreen.main.bounds.width - 32) / 2, height: 221)
        return layout
    }
    
    func makeFlashSaleCollectionView() -> UICollectionView {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: self.flashSaleCollectionLayout)
        collection.delegate = self
        collection.dataSource = self
        collection.backgroundColor = .clear
        collection.showsHorizontalScrollIndicator = false
        collection.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "DefaultCell")
        collection.register(FlashSaleCell.self, forCellWithReuseIdentifier: "FlashSaleCell")
        collection.translatesAutoresizingMaskIntoConstraints = false
        
        self.homeView.homeScrollView.addSubview(collection)

        collection.topAnchor.constraint(equalTo: self.homeView.homeScrollView.topAnchor, constant: 320).isActive = true
        collection.trailingAnchor.constraint(equalTo: self.homeView.homeScrollView.safeAreaLayoutGuide.trailingAnchor, constant: 12).isActive = true
        collection.leadingAnchor.constraint(equalTo: self.homeView.homeScrollView.leadingAnchor, constant: 12).isActive = true
        collection.heightAnchor.constraint(equalToConstant: 221).isActive = true
        
        return collection
    }
}
