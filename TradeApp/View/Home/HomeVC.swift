//
//  HomeVC.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 12.03.2023.
//

import UIKit

class HomeVC: UIViewController, ItemCoordinatingProtocol {
    
    var itemCoordinator: TabBarCoordinatingProtocol?
    
    var lastCategory: Int = 5
    var homeView: HomeView!
    var viewModel: HomeViewModelProtocol
    var categoriesDataSource: [(image: String, lable: String)]
    var categoriesCollectionLayout: UICollectionViewFlowLayout!
    var categoriesCollectionView: UICollectionView!
    var latestDealsCollectionLayout: UICollectionViewFlowLayout!
    var latestDealsCollectionView: UICollectionView!
    var flashSaleCollectionLayout: UICollectionViewFlowLayout!
    var flashSaleCollectionView: UICollectionView!
    
    // MARK: Отслеживание обновления данных и перезагрузка коллекций
    var latestDealsData: LatestDealsModel? {
        didSet {
            DispatchQueue.main.async {
                self.tryReloadCollectionData()
            }
        }
    }
    
    var flashSaleData: FlashSaleModel? {
        didSet {
            DispatchQueue.main.async {
                self.tryReloadCollectionData()
            }
        }
    }
    
    //MARK: ViewController Init
    required init(viewModel: HomeViewModelProtocol, categories: [(image: String, lable: String)], itemCoordinator: TabBarCoordinatingProtocol?) {
        self.viewModel = viewModel
        self.categoriesDataSource = categories
        self.itemCoordinator = itemCoordinator
        super .init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: Загрузка View
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.hideKeyboardWhenTappedAround()
        self.setupView()
    }
    
    //MARK: Подготовка к появлению View
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    //MARK: Появление View
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.activateDefaultCategoriesIcon(categoryNubmer: self.lastCategory)
    }
    
    //MARK: Подготовка к скрытию View
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.latestDealsData = nil
        self.flashSaleData = nil
    }
    
    // MARK: Получение данных HotSalesCollection
    private func getLatestDealsProducts() {
        self.viewModel.networkService.onComplitionLatestDeals = { [weak self] viewData in
            self?.latestDealsData = viewData
        }
    }
    
    // MARK: Получение данных HotSalesCollection
    private func getFlashSaleProducts() {
        self.viewModel.networkService.onComplitionFlashSale = { [weak self] viewData in
            self?.flashSaleData = viewData
        }
    }
    
    // MARK: Первоначальная настройка View
    private func setupView() {
        self.makeUpCollections()
        self.getLatestDealsProducts()
        self.getFlashSaleProducts()
    }

    // MARK: Cоздание коллекций и homeView
    private func makeUpCollections() {
        self.homeView = self.makeHomeView() as? HomeView
        self.categoriesCollectionLayout = self.makeCategoriesCollectionLayout()
        self.categoriesCollectionView = self.makeCategoriesCollectionView()
        self.latestDealsCollectionLayout = self.makeLatestDealsLayout()
        self.latestDealsCollectionView = self.makeLatestDealsCollectionView()
        self.flashSaleCollectionLayout = self.makeFlashSaleLayout()
        self.flashSaleCollectionView = self.makeFlashSaleCollectionView()
    }
    
    // MARK: Проверка полученных данных для коллекций
    private func tryReloadCollectionData() {
        guard let _ = self.latestDealsData, let _ = self.flashSaleData else { return }
        self.latestDealsCollectionView.reloadData()
        self.flashSaleCollectionView.reloadData()
    }

    // MARK: Активация первой категории (по умолчанию)
    private func activateDefaultCategoriesIcon(categoryNubmer: Int) {
        let indexPathForFirstRow = IndexPath(row: categoryNubmer, section: 0)
        self.categoriesCollectionView.selectItem(at: indexPathForFirstRow, animated: true, scrollPosition: [])
        self.collectionView(self.categoriesCollectionView, didSelectItemAt: indexPathForFirstRow)
    }
}
