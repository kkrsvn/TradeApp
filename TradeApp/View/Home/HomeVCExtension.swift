//
//  HomeVCExtension.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 13.03.2023.
//

import UIKit

extension HomeVC {
    
    func makeHomeView() -> UIView {
        let view = HomeView()
        view.delegate = self
        view.translatesAutoresizingMaskIntoConstraints = false
        self.view.addSubview(view)
        view.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        view.bottomAnchor.constraint(equalTo: self.view.bottomAnchor).isActive = true
        view.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        view.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        return view
    }
}

extension HomeVC: HomeViewDelegateProtocol {
    
    func didTapNavigationButton() {
        self.itemCoordinator?.tabBarCoordinator?.tabBarController.selectedIndex = 4
    }
}
