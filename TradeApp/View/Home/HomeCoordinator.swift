//
//  HomeCoordinator.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 29.03.2023.
//

import UIKit

class HomeCoordinator: CoordinatorProtocol, TabBarCoordinatingProtocol {
    
    var navigationController: UINavigationController?
    var tabBarCoordinator: TabBarCoordinatorProtocol?
    var childCoordinators: [CoordinatorProtocol] = []
    
    init(navigationController: UINavigationController?) {
        self.navigationController = navigationController
    }
    
    func start() {
        let homeVC: UIViewController & ItemCoordinatingProtocol = ModuleBuilder.createHomeModule(itemCoordinator: self)
        self.navigationController?.setViewControllers([homeVC], animated: true)
    }
}
