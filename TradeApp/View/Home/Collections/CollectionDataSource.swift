//
//  CollectionDataSource.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 16.03.2023.
//

import UIKit

extension HomeVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // MARK: - Categories Collection Count
        if collectionView == self.categoriesCollectionView {
            return self.categoriesDataSource.count
        }
        
        // MARK: - Latest Deals Collection Count
        else if collectionView == self.latestDealsCollectionView {
            return self.latestDealsData?.latestElements.count ?? 1
        }
        
        // MARK: - Flash Sale Collection Count
        else {
            return self.flashSaleData?.flashSaleElements.count ?? 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            // MARK: - Categories Collection View
            if collectionView == self.categoriesCollectionView {
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoriesCell", for: indexPath) as? CategoriesCell else {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DefaultCell", for: indexPath)
                    cell.backgroundColor = .systemRed
                    return cell
                }
                let image = self.categoriesDataSource[indexPath.row].image
                let text = self.categoriesDataSource[indexPath.row].lable
                let viewModel = CategoriesCell.CategoriesCollection(icon: image, lable: text)
                cell.setup(with: viewModel)
                return cell
            }
        
            // MARK: - Latest Deals Collection View
            else if collectionView == self.latestDealsCollectionView {
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "LetestDealsCell", for: indexPath) as? LatestDealsCell,
                      self.latestDealsData != nil
                else {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DefaultCell", for: indexPath)
                    cell.backgroundColor = UIColor(red: 0.702, green: 0.702, blue: 0.765, alpha: 1)
                    return cell
                }
                
                let title = self.latestDealsData?.latestElements[indexPath.row].title ?? ""
                let category = self.latestDealsData?.latestElements[indexPath.row].category ?? ""
                let imageURL = self.latestDealsData?.latestElements[indexPath.row].imageURL ?? ""
                let price = self.latestDealsData?.latestElements[indexPath.row].price ?? 0
                let latestViewModel = LatestDealsCell.LatestDealsCollection(title: title,
                                                                      category: category,
                                                                      picture: imageURL,
                                                                      price: price)
                cell.setup(with: latestViewModel)
                return cell
            }
        
            // MARK: - Flash Sale Collection View
            else {
                guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FlashSaleCell", for: indexPath) as? FlashSaleCell,
                      self.flashSaleData != nil
                else {
                    let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DefaultCell", for: indexPath)
                    cell.backgroundColor = UIColor(red: 0.702, green: 0.702, blue: 0.765, alpha: 1)
                    return cell
                }
                
                let title = self.flashSaleData?.flashSaleElements[indexPath.row].title ?? ""
                let category = self.flashSaleData?.flashSaleElements[indexPath.row].category ?? ""
                let imageURL = self.flashSaleData?.flashSaleElements[indexPath.row].imageURL ?? ""
                let price = self.flashSaleData?.flashSaleElements[indexPath.row].price ?? 0
                let discount = self.flashSaleData?.flashSaleElements[indexPath.row].discount ?? 0
                let flashViewModel = FlashSaleCell.FlashSaleCollection(title: title,
                                                                  category: category,
                                                                  picture: imageURL,
                                                                  price: price,
                                                                  discount: discount)
                cell.setup(with: flashViewModel)
                return cell
            }
    }
}
