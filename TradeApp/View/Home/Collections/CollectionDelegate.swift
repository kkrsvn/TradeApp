//
//  CollectionDelegate.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 16.03.2023.
//

import UIKit

extension HomeVC: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        // MARK: Активация выбранной категории
        if collectionView == self.categoriesCollectionView {
            if let cell = collectionView.cellForItem(at: indexPath) as? CategoriesCell {
                cell.changeIconColor(forChange: .active)
                self.lastCategory = Int(indexPath.row)
            }
            if self.lastCategory == 5,
               self.latestDealsData == nil,
               self.flashSaleData == nil {
                DispatchQueue.main.async {
                    self.viewModel.updateHomeView()
                }
            }
        } else {
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        // MARK: Деактивация категории
        if collectionView == self.categoriesCollectionView {
            if let cell = collectionView.cellForItem(at: indexPath) as? CategoriesCell {
                cell.changeIconColor(forChange: .passive)
            }
        } else {
            
        }
    }
}
