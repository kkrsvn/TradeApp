//
//  LatestDealsCell.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 19.03.2023.
//


import UIKit

protocol LatestDealsCellProtocol {
    var title: String { get set }
    var category: String { get set }
    var picture: String { get set }
    var price: Int { get set }
}

protocol LatestDealsSetupableProtocol {
    func setup(with viewModel: LatestDealsCellProtocol)
}

class LatestDealsCell: UICollectionViewCell {
    
    struct LatestDealsCollection: LatestDealsCellProtocol {
        var title: String
        var category: String
        var picture: String
        var price: Int
    }
    
    // MARK: Background Image
    private lazy var mainImage: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private lazy var mainImageConstraints = [
        self.mainImage.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 0),
        self.mainImage.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 0),
        self.mainImage.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0),
        self.mainImage.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: 0)
    ]
    
    // MARK: Background Category ImageView
    private lazy var categoryBackgroundImage: UIImageView = {
        let imageView = UIImageView()
        imageView.isHidden = true
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.backgroundColor = UIColor(red: 0.769, green: 0.769, blue: 0.769, alpha: 0.85)
        imageView.clipsToBounds = true
        imageView.layer.cornerRadius = 6
        return imageView
    }()
    
    private lazy var categoryBackgroundImageConstraints = [
        self.categoryBackgroundImage.bottomAnchor.constraint(equalTo: self.titleLable.topAnchor, constant: -6),
        self.categoryBackgroundImage.leadingAnchor.constraint(equalTo: self.priceLable.leadingAnchor),
        self.categoryBackgroundImage.heightAnchor.constraint(equalToConstant: 12),
        self.categoryBackgroundImage.widthAnchor.constraint(equalToConstant: 35)
    ]
    
    // MARK: Category Lable
    private lazy var categoryLable: UILabel = {
        let lable = UILabel()
        lable.isHidden = true
        lable.font = UIFont(name: "MarkPro-Bold", size: 6)
        lable.textColor = .black
        lable.numberOfLines = 0
        lable.textAlignment = .center
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var categoryLableConstraints = [
        self.categoryLable.centerYAnchor.constraint(equalTo: self.categoryBackgroundImage.centerYAnchor),
        self.categoryLable.centerXAnchor.constraint(equalTo: self.categoryBackgroundImage.centerXAnchor)
    ]
    
    // MARK: Title Lable
    private lazy var titleLable: OutlinedLabel = {
        let lable = OutlinedLabel()
        lable.isHidden = true
        lable.font = UIFont(name: "MarkPro-Bold", size: 12)
        lable.textColor = .white
        lable.outlineColor = .black
        lable.outlineWidth = 1
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var titleLableConstraints = [
        self.titleLable.bottomAnchor.constraint(equalTo: self.priceLable.topAnchor, constant: -5),
        self.titleLable.leadingAnchor.constraint(equalTo: self.priceLable.leadingAnchor)
    ]
    
    // MARK: Price Lable
    private lazy var priceLable: OutlinedLabel = {
        let lable = OutlinedLabel()
        lable.isHidden = true
        lable.font = UIFont(name: "MarkPro-Bold", size: 8)
        lable.textColor = .white
        lable.outlineColor = .black
        lable.outlineWidth = 1
        lable.numberOfLines = 0
        lable.textAlignment = .left
        lable.translatesAutoresizingMaskIntoConstraints = false
        return lable
    }()
    
    private lazy var priceLableConstraints = [
        self.priceLable.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -7),
        self.priceLable.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 7)
    ]
    
    // MARK: Add Button
    private lazy var addButton: UIButton = {
        let image = UIImage(systemName: "plus", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 10)))
        
        let button = UIButton()
        button.isHidden = true
        button.clipsToBounds = true
        button.layer.cornerRadius = 10
        button.backgroundColor = UIColor(red: 0.898, green: 0.914, blue: 0.937, alpha: 1)
        button.setImage(image, for: .normal)
        button.imageView?.tintColor = UIColor(red: 0.329, green: 0.333, blue: 0.537, alpha: 1)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var addButtonConstraints = [
        self.addButton.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: -5),
        self.addButton.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -5),
        self.addButton.widthAnchor.constraint(equalToConstant: 20),
        self.addButton.heightAnchor.constraint(equalToConstant: 20)
    ]
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.contentViewSets()
        self.setupView()
        self.setupConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView() {
        self.contentView.addSubview(self.mainImage)
        self.contentView.addSubview(self.categoryBackgroundImage)
        self.contentView.addSubview(self.categoryLable)
        self.contentView.addSubview(self.titleLable)
        self.contentView.addSubview(self.priceLable)
        self.contentView.addSubview(self.addButton)
    }

    private func setupConstraints() {
        NSLayoutConstraint.activate(self.mainImageConstraints)
        NSLayoutConstraint.activate(self.categoryBackgroundImageConstraints)
        NSLayoutConstraint.activate(self.categoryLableConstraints)
        NSLayoutConstraint.activate(self.titleLableConstraints)
        NSLayoutConstraint.activate(self.priceLableConstraints)
        NSLayoutConstraint.activate(self.addButtonConstraints)
    }
    
    private func contentViewSets() {
        self.contentView.clipsToBounds = true
        self.contentView.layer.cornerRadius = 16
    }
    
    private func showSubviews() {
        self.categoryBackgroundImage.isHidden = false
        self.categoryLable.isHidden = false
        self.titleLable.isHidden = false
        self.priceLable.isHidden = false
        self.addButton.isHidden = false
    }
}

extension LatestDealsCell: LatestDealsSetupableProtocol {
    func setup(with latestViewModel: LatestDealsCellProtocol) {
        guard let model = latestViewModel as? LatestDealsCollection else { return }
        self.categoryLable.text = model.category
        self.titleLable.text = model.title
        self.priceLable.text = "$" + String(model.price)
        guard let pictureURL = URL(string: model.picture) else { return }
        self.mainImage.load(url: pictureURL)
        self.showSubviews()
    }
}
