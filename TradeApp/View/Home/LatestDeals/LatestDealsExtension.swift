//
//  LatestDealsExtension.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 19.03.2023.
//

import UIKit

extension HomeVC {
    
    func makeLatestDealsLayout() -> UICollectionViewFlowLayout{
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 12
        layout.itemSize = CGSize(width: (UIScreen.main.bounds.width - 36) / 3, height: 150)
        return layout
    }
    
    func makeLatestDealsCollectionView() -> UICollectionView {
        let collection = UICollectionView(frame: .zero, collectionViewLayout: self.latestDealsCollectionLayout)
        collection.delegate = self
        collection.dataSource = self
        collection.backgroundColor = .clear
        collection.showsHorizontalScrollIndicator = false
        collection.register(UICollectionViewCell.self, forCellWithReuseIdentifier: "DefaultCell")
        collection.register(LatestDealsCell.self, forCellWithReuseIdentifier: "LetestDealsCell")
        collection.translatesAutoresizingMaskIntoConstraints = false
        
        self.homeView.homeScrollView.addSubview(collection)

        collection.topAnchor.constraint(equalTo: self.homeView.homeScrollView.topAnchor, constant: 127).isActive = true
        collection.trailingAnchor.constraint(equalTo: self.homeView.homeScrollView.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        collection.leadingAnchor.constraint(equalTo: self.homeView.homeScrollView.leadingAnchor, constant: 12).isActive = true
        collection.heightAnchor.constraint(equalToConstant: 150).isActive = true
        
        return collection
    }
}
