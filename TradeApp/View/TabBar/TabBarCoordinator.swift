//
//  TabBarCoordinator.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 29.03.2023.
//

import UIKit

class TabBarCoordinator: NSObject, CoordinatorProtocol, TabBarCoordinatorProtocol {
    
    var navigationController: UINavigationController?
    var tabBarController: UITabBarController
    var mainCoordinator: MainCoordinatorProtocol?
    var childCoordinators: [CoordinatorProtocol] = []
    
    var homeCoordinator: CoordinatorProtocol & TabBarCoordinatingProtocol
    var profileCoordinator: CoordinatorProtocol & TabBarCoordinatingProtocol
    
    init(tabBarController: UITabBarController, navigationController: UINavigationController?, mainCoordinator: MainCoordinatorProtocol?) {
        self.tabBarController = tabBarController
        self.mainCoordinator = mainCoordinator
        self.navigationController = navigationController
        
        self.homeCoordinator = HomeCoordinator(navigationController: UINavigationController())
        self.profileCoordinator = ProfileCoordinator(navigationController: UINavigationController())
    }
    
    func start() {
        self.homeCoordinator.tabBarCoordinator = self
        self.profileCoordinator.tabBarCoordinator = self
        
        self.homeCoordinator.start()
        self.profileCoordinator.start()
        
        self.childCoordinators.append(self.homeCoordinator)
        self.childCoordinators.append(self.profileCoordinator)
        
        var controllers: [UIViewController] = []
        controllers = items.map( { TabBarItem in
            switch TabBarItem {
            case .home:
                return self.homeCoordinator.navigationController ?? UINavigationController()
            case .favorite:
                return UIViewController()
            case .cart:
                return UIViewController()
            case .message:
                return UIViewController()
            case .profile:
                return self.profileCoordinator.navigationController ?? UINavigationController()
            }
        })
        
        self.tabBarController.setViewControllers(controllers, animated: true)
        self.tabBarController.viewControllers?.enumerated().forEach( { (index, viewController) in
            viewController.tabBarItem.image = items[index].image
        })
        self.tabBarController.tabBar.isTranslucent = true
        self.tabBarController.delegate = self
    }
}

extension TabBarCoordinator: UITabBarControllerDelegate {
    
}
