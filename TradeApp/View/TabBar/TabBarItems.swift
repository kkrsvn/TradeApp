//
//  TabBarItems.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 29.03.2023.
//

import UIKit

enum TabBarItem {
    case home
    case favorite
    case cart
    case message
    case profile
    
    var image: UIImage? {
        switch self {
        case .home:
            return UIImage(systemName: "house", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 15)))
        case .favorite:
            return UIImage(systemName: "heart", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 15)))
        case .cart:
            return UIImage(systemName: "cart", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 15)))
        case .message:
            return UIImage(systemName: "message", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 15)))
        case .profile:
            return UIImage(systemName: "person.crop.circle", withConfiguration: UIImage.SymbolConfiguration(font: UIFont.systemFont(ofSize: 15)))
        }
    }
}

let items: [TabBarItem] = [.home, .favorite, .cart, .message, .profile]
