//
//  TabBar.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 12.03.2023.
//

import UIKit

class TabBar: UITabBarController {
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.changeHeightOfTabbar()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
    }
    
    private func setUpView() {
        self.tabBar.tintColor = .black
        self.tabBar.unselectedItemTintColor = UIColor(red: 0.565, green: 0.565, blue: 0.565, alpha: 1)
        self.tabBar.backgroundColor = .white
        self.changeRadiusOfTabbar()
    }
    
    private func changeHeightOfTabbar(){
        var tabFrame = self.tabBar.frame
        tabFrame.size.height = 100
        tabFrame.origin.y = self.view.frame.size.height - tabFrame.size.height
        self.tabBar.frame = tabFrame
    }
    
    private func changeRadiusOfTabbar(){
        self.tabBar.layer.masksToBounds = true
        self.tabBar.isTranslucent = true
        self.tabBar.layer.cornerRadius = 30
        self.tabBar.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
    }
}
