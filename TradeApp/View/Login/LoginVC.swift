//
//  LoginVC.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 11.03.2023.
//

import UIKit
import CoreData

class LoginVC: UIViewController, CoordinatingProtocol {
    
    var persons: [Person] = []
    var context: NSManagedObjectContext!
    var coordinator: ChildrenCoordinatingProtocol?
    
    @IBOutlet weak var invalidDataLable: UILabel!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    required init(coordinator: ChildrenCoordinatingProtocol?) {
        self.coordinator = coordinator
        super .init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = false
        
        let fetchRequest: NSFetchRequest<Person> = Person.fetchRequest()

        do {
            self.persons = try self.context.fetch(fetchRequest)
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    private func blinkInvalideLable() {
        self.invalidDataLable.isHidden = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            self.invalidDataLable.isHidden = true
        }
    }

    @IBAction func loginTapButton(_ sender: Any) {
        
        guard let firstName = firstNameTextField.text,
              spaceValidate(string: firstName) else {
            isEmptyTextField(self.firstNameTextField, 0.35)
            return
        }
        
        guard self.persons.contains(where: { person in
            (person as Person).firstName == firstName
        }) else {
            self.blinkInvalideLable()
            return
        }
        
        self.coordinator?.mainCoordinator?.eventOccured(with: .moveToTabBar)
    }
    
    @IBAction func passwordHideTapButton(_ sender: Any) {
        self.passwordTextField.isSecureTextEntry.toggle()
    }
}
