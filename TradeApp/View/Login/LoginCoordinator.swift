//
//  LoginCoordinator.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 28.03.2023.
//

import UIKit

class LoginCoordinator: CoordinatorProtocol, ChildrenCoordinatingProtocol {
    
    var navigationController: UINavigationController?
    var mainCoordinator: MainCoordinatorProtocol?
    var childCoordinators: [CoordinatorProtocol] = []
    
    init(navigationController: UINavigationController?, mainCoordinator: MainCoordinatorProtocol?) {
        self.navigationController = navigationController
        self.mainCoordinator = mainCoordinator
    }
    
    func start() {
        let loginVC: UIViewController & CoordinatingProtocol = ModuleBuilder.creatLoginModule(coordinator: self)
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
}

