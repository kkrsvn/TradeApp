//
//  EmailValidate.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 11.03.2023.
//

import Foundation

func emailValidate(for email: String) -> Bool {
    let emailPattern = #"^\S+@\S+\.\S+$"#

    let emailRegex = try! NSRegularExpression(
        pattern: emailPattern,
        options: []
    )
    
    let sourceRange = NSRange(
        email.startIndex ..< email.endIndex,
        in: email
    )
    
    let result = emailRegex.matches(
        in: email,
        options: [],
        range: sourceRange
    )

    return !result.isEmpty
}
