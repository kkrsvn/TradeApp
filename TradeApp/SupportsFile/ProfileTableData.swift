//
//  ProfileTableData.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 12.03.2023.
//

import Foundation

struct ProfileData: ProfileTableViewCellPorotocol {
    var icon: String
    var lable: String
    var isHideArrow: Bool
    var isHideBalance: Bool
    
    init(icon: String, lable: String, isHideArrow: Bool, isHideBalance: Bool) {
        self.icon = icon
        self.lable = lable
        self.isHideArrow = isHideArrow
        self.isHideBalance = isHideBalance
    }
}

var profileDataSource: [ProfileData] = [
    ProfileData(icon: "creditcard", lable: "Trade Store", isHideArrow: false, isHideBalance: true),
    ProfileData(icon: "creditcard", lable: "Payment method", isHideArrow: false, isHideBalance: true),
    ProfileData(icon: "creditcard", lable: "Balance", isHideArrow: true, isHideBalance: false),
    ProfileData(icon: "creditcard", lable: "Trade History", isHideArrow: false, isHideBalance: true),
    ProfileData(icon: "arrow.triangle.2.circlepath", lable: "Restore Purchase", isHideArrow: false, isHideBalance: true),
    ProfileData(icon: "questionmark.circle", lable: "Help", isHideArrow: true, isHideBalance: true),
    ProfileData(icon: "iphone.and.arrow.forward", lable: "Log Out", isHideArrow: true, isHideBalance: true)
]

