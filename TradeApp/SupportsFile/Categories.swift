//
//  Categories.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 16.03.2023.
//

import Foundation

let categories: [(image: String, lable: String)] = [
    ("iphone.gen1", "Phones"),
    ("headphones", "Headphones"),
    ("gamecontroller", "Games"),
    ("car", "Car"),
    ("bed.double", "Furniture"),
    ("figure.2.and.child.holdinghands", "Kids")
]
