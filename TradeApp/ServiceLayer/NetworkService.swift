//
//  NetworkService.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 17.03.2023.
//

import Foundation

protocol NetworkServiceProtocol {
    var onComplitionLatestDeals: ((LatestDealsModel) -> Void)? { get set }
    var onComplitionFlashSale: ((FlashSaleModel) -> Void)? { get set }
    func fetchData(withURL url: String, forRequestType requestType: RequestType)
}

enum RequestType {
    case latestDeals
    case flashSale
}

class NetworkService: NetworkServiceProtocol {
    
    var onComplitionLatestDeals: ((LatestDealsModel) -> Void)?
    var onComplitionFlashSale: ((FlashSaleModel) -> Void)?
    
    func fetchData(withURL url: String, forRequestType requestType: RequestType) {
        self.performRequest(withURLString: url, forRequestType: requestType)
    }
    
    private func performRequest(withURLString urlString: String, forRequestType requestType: RequestType) {
        guard let url = URL(string: urlString) else { return }
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: url) { data, responce, error in
            if let data = data {
                switch requestType {
                case .latestDeals:
                    if let latestDealsModel = self.parseLatestDeelsJSON(withData: data) {
                        self.onComplitionLatestDeals?(latestDealsModel)
                    }
                case .flashSale:
                    if let flashSaleModel = self.parseFlashSaleSON(withData: data) {
                       self.onComplitionFlashSale?(flashSaleModel)
                   }
                }
            }
        }
        task.resume()
    }
    
    private func parseLatestDeelsJSON(withData data: Data) -> LatestDealsModel? {
        let decoder = JSONDecoder()
        do {
            let latestDealsData = try decoder.decode(LatestDealsData.self, from: data)
            guard let latestDealsModel = LatestDealsModel(latestDealsData: latestDealsData) else {
                return nil
            }
            return latestDealsModel
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return nil
    }
    
    private func parseFlashSaleSON(withData data: Data) -> FlashSaleModel? {
        let decoder = JSONDecoder()
        do {
            let flashSaleData = try decoder.decode(FlashSaleData.self, from: data)
            guard let flashSaleModel = FlashSaleModel(flashSaleData: flashSaleData) else {
                return nil
            }
            return flashSaleModel
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        return nil
    }
}
