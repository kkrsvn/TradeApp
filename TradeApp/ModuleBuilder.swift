//
//  ModuleBuilder.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 21.03.2023.
//

import UIKit

protocol ModuleBuilderProtocol: AnyObject {
    static func createSingInModule(coordinator: ChildrenCoordinatingProtocol?) -> UIViewController & CoordinatingProtocol
    static func creatLoginModule(coordinator: ChildrenCoordinatingProtocol?) -> UIViewController & CoordinatingProtocol
    static func createHomeModule(itemCoordinator: TabBarCoordinatingProtocol?) -> UIViewController & ItemCoordinatingProtocol
}

class ModuleBuilder: ModuleBuilderProtocol {
    
    static func createHomeModule(itemCoordinator: TabBarCoordinatingProtocol?) -> UIViewController & ItemCoordinatingProtocol {
        let networkService = NetworkService()
        let viewModel = HomeViewModel(networkService: networkService)
        let viewController = HomeVC(viewModel: viewModel, categories: categories, itemCoordinator: itemCoordinator)
        return viewController
    }
    
    static func createSingInModule(coordinator: ChildrenCoordinatingProtocol?) -> UIViewController & CoordinatingProtocol {
        
        let context = (UIApplication.shared.delegate as? AppDelegate)?.coreDataStack.persistentContainer.viewContext
        let viewController = SingInVC(coordinator: coordinator)
        viewController.context = context
        return viewController
    }
    
    static func creatLoginModule(coordinator: ChildrenCoordinatingProtocol?) -> UIViewController & CoordinatingProtocol {
        
        let context = (UIApplication.shared.delegate as? AppDelegate)?.coreDataStack.persistentContainer.viewContext
        let viewController = LoginVC(coordinator: coordinator)
        viewController.context = context
        return viewController
    }
}
