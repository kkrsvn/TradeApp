//
//  AppCoordinator.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 28.03.2023.
//

import UIKit

class AppCoordinator: CoordinatorProtocol, MainCoordinatorProtocol {

    var window: UIWindow?
    var navigationController: UINavigationController?
    var childCoordinators: [CoordinatorProtocol] = []
    
    init(window: UIWindow?) {
        self.window = window
    }
    
    func start() {
        self.showSingInViewController()
    }
    
    private func showSingInViewController() {
        self.navigationController = UINavigationController()
        self.window?.rootViewController = self.navigationController
        let singInCoordinator = SingInCoordinator(navigationController: self.navigationController, mainCoordinator: self)
        singInCoordinator.start()
        self.childCoordinators.append(singInCoordinator)
    }
    
    func eventOccured(with type: Event) {
        switch type {
        case .moveToLoginVC:
            self.showLoginViewController()
        case .moveToTabBar:
            self.showTabBarController()
        case .moveToSingInVC:
            self.showSingInController()
        }
    }
    
    private func showLoginViewController() {
        let loginCoordinator = LoginCoordinator(navigationController: self.navigationController, mainCoordinator: self)
        loginCoordinator.start()
        childCoordinators.append(loginCoordinator)
    }
    
    private func showTabBarController() {
        let tabBarController = TabBar()
        self.window?.rootViewController = tabBarController
        let tabBarCoordinator = TabBarCoordinator(tabBarController: tabBarController, navigationController: self.navigationController,  mainCoordinator: self)
        tabBarCoordinator.start()
        childCoordinators.append(tabBarCoordinator)
    }
    
    private func showSingInController() {
        self.window?.rootViewController = self.navigationController
        self.navigationController?.popToRootViewController(animated: true)
    }
}
