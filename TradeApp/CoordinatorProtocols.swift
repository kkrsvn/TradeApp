//
//  CoordinatorProtocols.swift
//  TradeApp
//
//  Created by Kirill Krasavin on 28.03.2023.
//

import UIKit

enum Event {
    case moveToLoginVC
    case moveToTabBar
    case moveToSingInVC
}

protocol CoordinatorProtocol {
    var navigationController: UINavigationController? { get set }
    var childCoordinators: [CoordinatorProtocol] { get set }
    
    func start()
}

protocol MainCoordinatorProtocol {
    var window: UIWindow? { get set } 
    func eventOccured(with type: Event)
}

protocol ChildrenCoordinatingProtocol {
    var mainCoordinator: MainCoordinatorProtocol? { get set  }
}

protocol CoordinatingProtocol {
    var coordinator: ChildrenCoordinatingProtocol? { get set  }
}

protocol TabBarCoordinatorProtocol {
    var mainCoordinator: MainCoordinatorProtocol? { get set  }
    var tabBarController: UITabBarController { get set }
}

protocol TabBarCoordinatingProtocol {
    var tabBarCoordinator: TabBarCoordinatorProtocol? { get set  }
}

protocol ItemCoordinatingProtocol {
    var itemCoordinator: TabBarCoordinatingProtocol? { get set  }
}
